var app = require('express')();
var server = require('http').Server(app);
var io = require('socket.io')(server);

var allowCrossDomain = function (req, res, next) {
    res.header("Access-Control-Allow-Origin", "*");
    res.header("Access-Control-Allow-Methods", "GET,PUT,POST,DELETE,OPTION");
    res.header("Access-Control-Allow-Headers", "Content-Type");
    res.header("Access-Control-Allow-Credentials", "true");
    next();
};

app.use(allowCrossDomain);

server.listen(3001);

app.get('/', function (req, res) {
    res.sendfile(__dirname + '/public/index.html');
});

io.on("connection", function (socket) {

    var session_id = socket.request._query.token;

    socket.join(session_id);

    socket.on('disconnect', function() {
        socket.leave(session_id);
    });

    socket.on("notify", function () {
        socket.broadcast.to(session_id).emit('reload');
    });
});
